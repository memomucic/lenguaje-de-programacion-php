<?php 
	if(isset($_POST['submit']))
	{
        //print_r($_POST);
		$em= $_POST['email'];
        $cu= $_POST['curp'];
        $pa= $_POST['palabras'];
        $sim= $_POST['simbolos'];
        $nu= $_POST['numeros'];

        $er1="/\S+@\S+\.\S+/";
        $er2="/[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[0-9]{2}/";
        $er3="/^[a-zA-Z]{51,100}$/";
       // $er4="/[ @.*+\-?^{}()\\ ]/";
        $er5="/^[0-9].[0-9]/";
     
		
		
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Expresiones Regulares con PHP</title>
    <style type="text/css">
        *{
            margin:0;
            padding:0;

            box-sizing: border-box;

        }
        .formulario{
            width: 600px;
            height:100%;
            background: #4e4d4d;
            margin: auto;
            margin-top:180px;
            box-shadow: 7px 13px 37px #000;
            padding: 20px 30px;
            border-top: 4px solid #017bab;
            color: white;
        }
            body{
                font-family: arial;
            }

        .controles{
            width:100%;
            border:1px solid #017bab;
            margin-bottom: 15px;
            padding:11px 10px;
            background: #252323;
            font-size:14px;
            font-weight: bold;
            color: white;
        }

        .boton{
            width:100%;
            height:40px;
            background: #017bab;
            border:none;
            color: white;
            margin-botom:16px;
        }

  </style>
</head>
<body>

<section   class="formulario" >
<form method="post" action="expresiones_regulares.php" enctype="multipart/form-data>
				<!-- email -->

				<label class="form-label" for="input-text">Email: </label>
				<input name="email" class="controles " type="text" id="id_email" placeholder="Digite email ">
              
                <?php
                $a = preg_match($er1,$em);
                var_dump($a);
                ?>
                
                <br>
                <br>
                <!-- CURP -->
				<label class ="form-label" for="input-text">Curp: </label>
				<input name="curp" class="controles " type="text" id="curp" placeholder="Digite curp">
                <?php
                $b = preg_match($er2,$cu);
                var_dump($b);
                ?>
                <br>
                <br>
				
			
				<!-- palabras de longitud mayor a 50 -->
				<label class="form-label" for="input-text">palabras de longitud mayor a 50</label>
				<input name="palabras"class="controles " type="text" id="id_palabras" placeholder="Digite palabra">
				<?php
                $c = preg_match($er3,$pa);
                var_dump($c);
                ?>
                <br>
                <br>
                <!-- simbolos especiales  -->
				<label class="form-label" for="input-text">Simbolos especiales</label>
				<input name="simbolos" class="controles "type="text" id="id_simbolos" placeholder="Digite cadena">
                <?php
              
                $d = preg_quote($sim,'/');
                var_dump ($d);
                ?>
                <br>
                <br>
				<!-- Números decimales  -->
				<label class="form-label" for="input-text">Detectar números decimales</label>
				<input name="numeros" class="controles " type="text" id="id_numeros" placeholder="Digite cadena">
                <?php
                $e = preg_match($er5,$nu);
                var_dump($e);
                ?>
                <br>
                <br>


				<!-- Botones -->
				<input type='submit' name="submit" class="boton" value="Evaluar"/>
                <br><br>
				<input type='reset' class="boton" value="limpiar"/>



			</form>
          

</section>





</body>
</html>