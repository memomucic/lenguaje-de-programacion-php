<?php 
session_start();
$estado=FALSE;
$tam= count($_SESSION['num_cta']);


for ($x=0 ; $x < $tam ; $x++)
{
	if( $_SESSION['sesion_iniciada'][$x]){

		$estado =true;
	}
}
if($estado==FALSE)
	{
		header('Location: login.php');
	}
?>

<?php 
	if(isset($_POST['submit']))
	{
		$_SESSION['sesion_iniciada'][$tam]=FALSE;
		$_SESSION['num_cta'][$tam] = $_POST['num_cta'];
		$_SESSION['nombre'][$tam] = $_POST['nombre'];
		$_SESSION['primer_apellido'][$tam] = $_POST['primer_apellido'];
		$_SESSION['segundo_apellido'][$tam] = $_POST['segundo_apellido'];
		
		$_SESSION['genero'][$tam] =$_POST['genero'];
		$_SESSION['fec_nac'][$tam] =$_POST['fec_nac'];
		$_SESSION['contrasena'] [$tam]= $_POST['contrasena'];
		
	}
	
?>



<html>
<head>
    <title>Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
</head>
    <body>
	<header>
	<a href='./info.php'>Ver informacion</a>
	<a href='./login.php'>Cerrar sesion Sesion</a>
  </header>
	<div class="container">
		<div class="columns">
			<!--
				En la etiqueta form son importantes los siguientes atributos method y action
				method para especificar el método en que se va a enviar el formulario "get" o "post" cambien el tipo de metodo para ver cambios
				action para especificar el archivo que va a procesar la información
			-->
			<!--<form action="info.php?accion=get&texto=textoenget" method="POST">-->

			<form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF'])?>" enctype="multipart/form-data>
				<!-- numero de cuenta -->
				<label class="form-label" for="input-text">numero de cuenta: </label>
				<input name="num_cta" class="form-input " type="text" id="id_num_cta" placeholder="Numero de cuenta">
				<!-- Nombre -->
				<label class="form-label" for="input-text">Nombre</label>
				<input name="nombre" class="form-input " type="text" id="id_nombre" placeholder="Nombre">
			
				<!-- primer apellido -->
				<label class="form-label" for="input-text">Apellido 1</label>
				<input name="primer_apellido" class="form-input " type="text" id="id_primer_apellido" placeholder="Apellido1">
				<!-- segundo apellido -->
				<label class="form-label" for="input-text">Apellido 2</label>
				<input name="segundo_apellido" class="form-input " type="text" id="id_segundo_apellido" placeholder="Apellido2">
				<!--Genero -->
				<label class="form-label">Genero</label>
				<label class="form-radio">
					<input type="radio" name="genero" value="H" checked>
					<i class="form-icon"></i> Hombre
				</label>
				<label class="form-radio">
					<input type="radio" name="genero" value="M">
					<i class="form-icon"></i> Mujer
				</label>
				<label class="form-radio">
					<input type="radio" name="genero" value="O">
					<i class="form-icon"></i> Otro
				</label>
				
				<!--Fecha de nacimiento  -->
				<label class="form-label" for="input-date">Fecha</label>
				<input name="fec_nac" class="form-input " type="date" id="fec_nac"
					   placeholder="fecha de nacimiento">


				<!-- form password control -->
				<label class="form-label" for="input-password">Contraseña</label>
				<input name="contrasena" class="form-input" type="password" id="contrasena"
					   placeholder="Contraseña">
	

				<!-- Botones -->
				<input type='submit' name="submit" class="btn" value="Enviar"/>
				<input type='reset' class="btn btn-primary" value="limpiar"/>



			</form>
		</div>
	</div>

	

    </body>
</html>